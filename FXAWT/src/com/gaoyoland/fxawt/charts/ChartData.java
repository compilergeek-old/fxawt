package com.gaoyoland.fxawt.charts;

public class ChartData {

	private String tag;
	private int value;

	public ChartData(String tag, int value) {
		this.tag = tag;
		this.value = value;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}
}
