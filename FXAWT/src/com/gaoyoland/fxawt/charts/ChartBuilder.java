package com.gaoyoland.fxawt.charts;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.chart.PieChart;

public class ChartBuilder {

	public static PieChart getPieChart(ChartData... data) {
		ObservableList<PieChart.Data> chartData = FXCollections.observableArrayList();
		for (ChartData piece : data) {
			chartData.add(new PieChart.Data(piece.getTag(), piece.getValue()));
		}

		PieChart piechart = new PieChart(chartData);
		return piechart;
	}

	/*
	 * public static LineChart<Number, Number> getLineChart(String xAxisName,
	 * String yAxisName, OrderedPairData... data){
	 * 
	 * final NumberAxis xAxis = new NumberAxis(); final NumberAxis yAxis = new
	 * NumberAxis();
	 * 
	 * LineChart<Number, Number> lineChart = new LineChart<Number, Number>();
	 * lineChart.getData().add(series);
	 * 
	 * }
	 */

}
