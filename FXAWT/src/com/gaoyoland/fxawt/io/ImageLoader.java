package com.gaoyoland.fxawt.io;

import java.io.File;

import javafx.scene.image.Image;

public class ImageLoader {

	public static Image loadImage(String filepath) {
		return new Image("file:" + filepath);
	}

	public static Image loadImage(File filepath) {
		return new Image("file:" + filepath.getAbsolutePath());
	}

}
