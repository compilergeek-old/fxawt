package com.gaoyoland.fxawt;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;

/**
 * Not event based input management
 *
 */
public class InputManager {

	private static double mouseX;
	private static double mouseY;
	private static boolean isPressed;
	private static boolean isRightPressed;
	private static boolean isMiddlePressed;
	private static List<KeyCode> currentlyPressed = Collections.synchronizedList(new ArrayList<KeyCode>());

	/**
	 * @deprecated
	 */
	public static String impl_debugInfo() {
		return mouseX + "," + mouseY + "," + isPressed + "," + isMiddlePressed + "," + isRightPressed + ","
				+ currentlyPressed;
	}

	public static double getX() {
		return mouseX;
	}

	public static double getY() {
		return mouseY;
	}

	public static boolean isPressed() {
		return isPressed;
	}

	public static boolean isRightPressed() {
		return isRightPressed;
	}

	public static boolean isMiddlePressed() {
		return isMiddlePressed;
	}

	public static List<KeyCode> getAllPressed() {
		return currentlyPressed;
	}

	public static boolean isKeyPressed(KeyCode code) {
		return currentlyPressed.contains(code);
	}

	private static void setMouseX(double mouseX) {
		InputManager.mouseX = mouseX;
	}

	private static void setMouseY(double mouseY) {
		InputManager.mouseY = mouseY;
	}

	private static void setPressed(boolean isPressed) {
		InputManager.isPressed = isPressed;
	}

	private static void setRightPressed(boolean isRightPressed) {
		InputManager.isRightPressed = isRightPressed;
	}

	private static void setMiddlePressed(boolean isMiddlePressed) {
		InputManager.isMiddlePressed = isMiddlePressed;
	}

	private static void addPressed(KeyCode code) {
		if (!currentlyPressed.contains(code)) {
			currentlyPressed.add(code);
		}
	}

	private static void removePressed(KeyCode code) {
		currentlyPressed.remove(code);
	}

	protected static void init(Scene scene) {

		scene.setOnMouseMoved(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				InputManager.setMouseX(event.getX());
				InputManager.setMouseY(event.getY());
			}

		});

		scene.setOnMousePressed(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				if (event.getButton() == MouseButton.PRIMARY) {
					InputManager.setPressed(true);
				}
				if (event.getButton() == MouseButton.SECONDARY) {
					InputManager.setRightPressed(true);
				}
				if (event.getButton() == MouseButton.MIDDLE) {
					InputManager.setMiddlePressed(true);
				}

			}

		});

		scene.setOnMouseReleased(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				if (event.getButton() == MouseButton.PRIMARY) {
					InputManager.setPressed(false);
				}
				if (event.getButton() == MouseButton.SECONDARY) {
					InputManager.setRightPressed(false);
				}
				if (event.getButton() == MouseButton.MIDDLE) {
					InputManager.setMiddlePressed(false);
				}
				if (event.getButton() == MouseButton.NONE) {
					InputManager.setPressed(false);
					InputManager.setRightPressed(false);
					InputManager.setMiddlePressed(false);
				}

			}

		});

		scene.setOnKeyPressed(new EventHandler<KeyEvent>() {
			@Override
			public void handle(KeyEvent event) {
				InputManager.addPressed(event.getCode());
			}
		});

		scene.setOnKeyReleased(new EventHandler<KeyEvent>() {
			@Override
			public void handle(KeyEvent event) {
				InputManager.removePressed(event.getCode());
			}
		});
	}

}
