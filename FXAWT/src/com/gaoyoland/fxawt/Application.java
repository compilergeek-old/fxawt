package com.gaoyoland.fxawt;

public interface Application {
	public void event(Graphics g);

	public void render(Graphics g);

}
