package com.gaoyoland.fxawt.test;

import com.gaoyoland.fxawt.FXAWTApplication;
import com.gaoyoland.fxawt.Graphics;
import com.gaoyoland.fxawt.InputManager;
import com.gaoyoland.fxawt.charts.ChartBuilder;
import com.gaoyoland.fxawt.charts.ChartData;

import javafx.scene.chart.PieChart;
import javafx.scene.input.KeyCode;
import javafx.stage.Stage;

public class TestApp extends FXAWTApplication {

	int rot = 0;
	ChartData[] data = new ChartData[] { new ChartData("Banana", 15), new ChartData("Apples", 62),
			new ChartData("Chocolate", 25) };

	public TestApp() {
		super(500, 500, "Test", false);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		super.start(primaryStage);
		setUserAgentStylesheet(STYLESHEET_MODENA);
	}

	@Override
	public void render(Graphics g) {
		super.render(g);

		PieChart chart = ChartBuilder.getPieChart(data);
		chart.setAnimated(true);
		chart.setTitle("Sales Chart");
		g.draw(chart);

		if (InputManager.isKeyPressed(KeyCode.UP)) {
			data[0] = new ChartData("Banana", data[0].getValue() + 1);
		}

		// g.setRotation(rot);
		// g.drawRect(200, 200, 50, 50);
		// rot++;
		g.resetVariables();
	}

	public static void main(String[] args) {
		launch(args);
	}

}
