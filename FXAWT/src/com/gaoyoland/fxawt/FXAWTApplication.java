package com.gaoyoland.fxawt;

import javafx.animation.AnimationTimer;
import javafx.scene.Camera;
import javafx.scene.Group;
import javafx.scene.ParallelCamera;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class FXAWTApplication extends javafx.application.Application implements Application {

	private InternalRenderer render = new InternalRenderer();
	private Graphics graphics = new Graphics(render);
	private boolean init = false;

	protected final Group root = new Group();
	protected final int width;
	protected final int height;
	protected String title;
	protected Scene scene;
	protected Camera camera;

	private AnimationTimer timer = new AnimationTimer() {
		@Override
		public void handle(long now) {

			event(graphics);
			render(graphics);
		}

	};

	public FXAWTApplication(int width, int height, String title, boolean scene3D) {
		super();
		this.width = width;
		this.height = height;
		this.title = title;
		camera = new ParallelCamera();
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		scene = new Scene(root, width, height, true);
		primaryStage.setTitle(title);
		primaryStage.setScene(scene);
		primaryStage.show();

		scene.setCamera(camera);
		InputManager.init(scene);

		timer.start();
	}

	public void init() {
		init(graphics);
	}

	public void init(Graphics g) {
	}

	@Override
	public void event(Graphics g) {

	}

	@Override
	public void render(Graphics g) {
		render.process(root);
		if (!init) {
			render.finishInit(root);
			init = true;
		}
	}

}
