package com.gaoyoland.fxawt;

import java.util.concurrent.CopyOnWriteArrayList;

import javafx.scene.Group;
import javafx.scene.Node;

class InternalRenderer {

	private CopyOnWriteArrayList<Node> nodes = new CopyOnWriteArrayList<Node>();
	private CopyOnWriteArrayList<Node> persistantNodes = new CopyOnWriteArrayList<Node>();

	public void addNode(Node node) {
		nodes.add(node);
	}

	public void process(Group root) {
		root.getChildren().clear();
		root.getChildren().addAll(nodes);
		nodes.clear();
	}

	/**
	 * A persistant node is a node that does not get cleared
	 * 
	 * @param node
	 */
	public void addPersistantNode(Node node) {
		persistantNodes.add(node);
	}

	/**
	 * This is called after the init method
	 * 
	 * @param root
	 */
	public void finishInit(Group root) {
		root.getChildren().addAll(persistantNodes);
	}

}
