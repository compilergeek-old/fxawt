package com.gaoyoland.fxawt;

import javafx.geometry.Point3D;
import javafx.scene.Node;
import javafx.scene.paint.Color;
import javafx.scene.paint.Material;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Arc;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import javafx.scene.shape.Shape3D;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.transform.Rotate;

/**
 * Comparable to {@link java.awt.Graphics} and drives most of the drawing
 *
 */
public class Graphics {

	// TODO Rounded Rectangles, ArcType for Arcs
	private InternalRenderer render;
	private Material material;
	private Paint paint;
	private double opacity;
	private double strokeWidth;
	private boolean smooth;
	private double scaleX;
	private double scaleY;
	private double scaleZ;
	private Font font;
	private boolean fill;
	private double rotation;
	private Point3D axis;

	protected Graphics(InternalRenderer render) {
		this.render = render;
		resetVariables();
	}

	public void resetVariables() {
		this.opacity = 100;
		this.strokeWidth = 1;
		this.scaleX = 1;
		this.scaleY = 1;
		this.scaleZ = 1;
		this.smooth = true;
		this.fill = false;
		this.material = null;
		this.paint = Color.BLACK;
		this.rotation = 0;
		this.axis = Rotate.Z_AXIS;
	}

	private Node setVars(Node node) {
		if (node instanceof Shape3D) {
			Shape3D s3d = (Shape3D) node;
			if (material != null) {
				s3d.setMaterial(material);
			}
			s3d.setRotate(rotation);
			s3d.setRotationAxis(axis);
		}

		if (node instanceof Shape) {
			Shape s2d = (Shape) node;
			s2d.setStrokeWidth(strokeWidth);
			s2d.setSmooth(smooth);
			if (paint != null) {
				if (fill) {
					s2d.setFill(paint);
					s2d.setStroke(paint);
				} else {
					s2d.setStroke(paint);
					s2d.setFill(null);
				}
			}
			s2d.setRotationAxis(axis);
			s2d.setRotate(rotation);
		}

		if (node instanceof Text) {
			Text text = (Text) node;
			if (font != null) {
				text.setFont(font);
			}
		}

		node.setScaleX(scaleX);
		node.setScaleY(scaleY);
		node.setScaleZ(scaleZ);
		node.setOpacity(opacity);
		return node;
	}

	public void drawRect(double x, double y, double width, double height) {
		render.addNode(setVars(getRect(x, y, width, height)));
	}

	public Rectangle getRect(double x, double y, double width, double height) {
		Rectangle box = new Rectangle(x, y, width, height);
		return box;
	}

	public void drawArc(double x, double y, double radiusX, double radiusY, double startAngle, double length) {
		render.addNode(setVars(getArc(x, y, radiusX, radiusY, startAngle, length)));
	}

	public Arc getArc(double x, double y, double radiusX, double radiusY, double startAngle, double length) {
		Arc arc = new Arc(x, y, radiusX, radiusY, startAngle, length);
		return arc;
	}

	public void drawCircle(double x, double y, double radius) {
		render.addNode(setVars(getCircle(x, y, radius)));
	}

	public Circle getCircle(double x, double y, double radius) {
		Circle cir = new Circle(x, y, radius);
		return cir;
	}

	public void drawLine(double x, double y, double x2, double y2) {
		render.addNode(getLine(x, y, x2, y2));
	}

	public Line getLine(double x, double y, double x2, double y2) {
		Line line = new Line(x, y, x2, y2);
		return line;
	}

	public void drawString(double x, double y, String string) {
		render.addNode(setVars(getString(x, y, string)));
	}

	public Text getString(double x, double y, String string) {
		Text text = new Text(x, y, string);
		return text;
	}

	public void drawPersistant(Node node) {
		render.addPersistantNode(node);
	}

	public void draw(Node node) {
		render.addNode(node);
	}

	public void setMaterial(Material material) {
		this.material = material;
	}

	public void setPaint(Paint paint) {
		this.paint = paint;
	}

	public void setStrokeWidth(double width) {
		this.strokeWidth = width;
	}

	public void setSmooth(boolean smooth) {
		this.smooth = smooth;
	}

	public void setFill(boolean fill) {
		this.fill = fill;
	}

	public void setScale(double x, double y, double z) {
		this.scaleX = x;
		this.scaleY = y;
		this.scaleZ = z;
	}

	public void setFont(Font f) {
		this.font = f;
	}

	public void setRotation(double rotation) {
		this.rotation = rotation;
	}

	public void setRotationalAxis(Point3D axis) {
		this.axis = axis;
	}

	public void setColor(Paint paint) {
		setPaint(paint);
	}

}
